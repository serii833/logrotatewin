﻿namespace logrotate
{
    public class Strings
    {
          public static string Copying = "Copying";
  public static string CopyRight = "Copyright (C) 2012-2013 Ken Salter";
  public static string CouldNotBeFound = "could not be found";
  public static string DebugOptionSet = "Debug option set to true";
  public static string DeletingFile = "Deleting file";
  public static string Executing = "Executing";
  public static string ExecutingFirstAction = "Executing FirstAction commands";
  public static string ExecutingLastAction = "Executing LastAction commands";
  public static string ExecutingPostRotateSharedScripts = "Executing Postrotate (SharedScripts) commands";
  public static string ExecutingPreRotateSharedScripts = "Executing Prerotate (SharedScripts) commands";
  public static string ExtInTaboo = "extension is in taboo list";
  public static string ForceOptionRotate = "Force option is set, will rotate";
  public static string ForceOptionSet = "Force option set to true";
  public static string GlobalOptionsAboveSections = "Global options must go above sections, ignoring";
  public static string GNURights = "This may be freely redistributed under the terms of the GNU Public License.  ";
  public static string MailOptionSet = "-m/--mail option not supported, ignoring";
  public static string NoTimestampDirectives = "No timestamp directives are set true, skipping timestamp check";
  public static string ProcessInclude = "Process include directive file";
  public static string Processing = "Processing";
  public static string ProgramName = "logrotate";
  public static string Renaming = "Renaming";
  public static string ShreddingFile = "Shredding file";
  public static string Skipping = "Skipping";
  public static string To =  "to ";
  public static string UnknownCmdLineArg = "Unknown command line argument";
  public static string UpdateStatus = "Updating rotation date in status file";
  public static string Usage1 = "Usage: logrotate [-d|--debug] [-f|--force]";
  public static string Usage2 = "[-s|--state=statefile] [-v|--verbose] [-?|--help] [--usage]";
  public static string Usage3 = "<configfile>";
  public static string VerboseOptionSet = "Verbose option set to true";
  public static string AddingConfigFile = "Adding to config files to process";
  public static string AlternateStateFile = "Setting alternate state file to";
  public static string Comment = "comment";
  public static string Compressing = "Compressing rotated log file";
  public static string CreateNewEmptyLogFile = "Creating new empty log file";
  public static string LogFileEmpty = "Log file is empty";
  public static string NoRotateNotGTMinimumFileSize = "Will not rotate since log file is not greater than minimum file size";
  public static string RotateWhenMaximumFileSize = "Will rotate since log file is greater than maximum file size";
  public static string ParseConfigFile = "Parsing configuration file";
  public static string RotateBasedonFileSize = "Rotate file based on file size";
  public static string RotateSimulated = "Rotate will be simulated because Debug is true";
  public static string RotatingFile = "Rotating file";
  public static string Setting = "Setting";
  public static string TruncateLogFile = "Truncating original log file";
  public static string UnknownDirective = "Unknown directive";
  public static string UnknownSizeType = "Unknown size type";
  public static string NewSection = "new section";
  public static string ReadLine = "read line";
  public static string SendingEmailTo = "Sending email to";
  public static string LookingForFolders = "Looking for folders found in";
  public static string MatchedFolder = "Matched folder";
  public static string Matching = "matching";
  public static string NoFilesFound = "No files found in";
  public static string NoFoldersFound = "No folders found in";
  public static string WithWildcardPattern = "with wildcard pattern";
  public static string AddingFileToRotate = "Adding files to rotate";
  public static string File = "File";
  public static string Key = "Key";
  public static string TimestampDirectives = "Timestamp directives are set true, doing timestamp checks";
  public static string NoRotateNotGTTimeStamp = "Will not rotate since log file timestamp did not meet check policy or it was already rotated.";
  public static string NoStatusDate = "No date found in status file for path";
  public static string StateFileLocation = "Status file located in";
  public static string ProcessingGlobalDirective = "Procesing directives included in the global settings";
  public static string Section = "section(s)";
  public static string ExecutionOutput = "Execution output";
    }
}